import sys
# 控制台调用方法时需将项目根目录添加到环境变量
sys.path.append('../')
from libs.untils.functions import *
from libs.untils.functions import _get_window_id_with_pid
import time
import shutil

def res_check(result,pass_print,fail_print):
    """
    断言方法 判断result是否为True
    Args:
        result: 需要检查的结果
        pass_print: result为True时打印的信息
        fail_print: result为False时打印的信息

    Returns:

    """
    if result:
        print(pass_print)
    else:
        print(fail_print)
        sys.exit()

if __name__ == '__main__':
    #----------------------------------------------------初始化参数------------------------------------------------------
    # 项目根目录绝对路径
    pwd_path = os.path.abspath(os.path.dirname(__file__))
    root_path = os.path.dirname(pwd_path)
    # 截图文件夹路径
    screenshot_dir = f'{root_path}/screenshot'
    # 时间戳
    times = str(int(time.time()))
    #----------------------------------------------打开置顶应用并初始化检查-------------------------------------------------
    #exec_res = restart_app('xdotool exec kolourpaint')
    #res_check(exec_res , '应用启动成功', '应用启动失败')
    #time.sleep(2)
    # 获取窗口ID
    #window_id = int(_get_window_id_with_pid(9418))
    window_id = int(get_window_id_with_window_name('KolourPaint'))
    #判断是否获取到窗口ID
    res_check(window_id, f'获取窗口ID成功{window_id}', f'获取窗口ID失败{window_id}')
    # 置顶窗口
    top_window_res = top_window(window_id)
    # 判断是否置顶成功
    res_check(top_window_res, '置顶窗口成功', '置顶窗口失败')
    mouse_move_absolute(36,345)
    mouse_click('left',repeat=1)
    #大圆
    mouse_move_absolute(150,300)
    mouse_down('left')
    mouse_move_absolute(380,530)
    mouse_up('left')
    #小圆
    mouse_move_absolute(185,335)
    mouse_down('left')
    mouse_move_absolute(345,495)
    mouse_up('left')
    #八角形
    mouse_move_absolute(14,200)
    mouse_click('left',repeat=1)
    mouse_move_absolute(265,335)
    mouse_down('left')
    mouse_move_absolute(241,358)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(208,358)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(208,391)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(185,415)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(208,438)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(208,471)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(241,471)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(265,495)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(288,471)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(321,471)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(321,438)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(345,415)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(322,391)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(322,358)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(289,358)
    mouse_up('left')
    mouse_down('left')
    mouse_move_absolute(265,335)
    mouse_up('left')
    #文字
    mouse_move_absolute(39,175)
    mouse_click('left',repeat=1)
    mouse_move_absolute(218,391)
    mouse_down('left')
    mouse_move_absolute(311,438)
    mouse_up('left')
    mouse_move_absolute(220,400)
    mouse_click('left',repeat=1)
    key_input('n')
    key_input('a')
    key_input('n')
    key_input('k')
    key_input('a')
    key_input('i')
    key_input('1')   
    mouse_move_absolute(1083,104)
    time.sleep(1)
    mouse_click('left',repeat=1)
    time.sleep(1)
    mouse_move_absolute(859,145)
    mouse_click('left',repeat=1)
    mouse_move_absolute(840,720)
    mouse_click('left',repeat=1)
    time.sleep(1)
    #日期
    mouse_move_absolute(39,175)
    mouse_click('left',repeat=1)
    mouse_move_absolute(235,497)
    mouse_down('left')
    mouse_move_absolute(305,520)
    mouse_up('left')
    mouse_move_absolute(250,500)
    mouse_click('left',repeat=1)
    key_input('1')
    key_input('9')
    key_input('1')
    key_input('9')
    time.sleep(1)
    mouse_move_absolute(1083,104)
    time.sleep(1)
    mouse_click('left',repeat=2)
    mouse_move_absolute(859,150)
    time.sleep(1)
    mouse_click('left',repeat=1)
    time.sleep(1)
    mouse_move_absolute(840,570)
    mouse_click('left',repeat=1)

    mouse_move_absolute(600,400)
    mouse_click('left',repeat=1)



