import requests
import json
import cv2
import base64

def cv2_to_base64(image):
    data = cv2.imencode('.jpg', image)[1]
    return base64.b64encode(data.tostring()).decode('utf8')

# 发送HTTP请求
data = {'images':[cv2_to_base64(cv2.imread("./2022-05-07-1651920347_1087x694.png"))]}
headers = {"Content-type": "application/json"}
url = "http://127.0.0.1:8866/predict/chinese_ocr_db_crnn_server"
r = requests.post(url=url, headers=headers, data=json.dumps(data))

def isExistWord(str):
    for item in r.json()["results"][0]["data"]:
        if item["text"]==str:
            return True
    return False
    
#def check_coordination(str,list)
